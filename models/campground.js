const mongoose = require('mongoose')
const Comment = require('./comment')

//set up schema
let campSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String,
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Comment'
        }
    ],
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
})

//pre-hook to also target associated comments when deleting campgrounds
campSchema.pre('remove', async function() {
	await Comment.remove({
		_id: {
			$in: this.comments
		}
	});
});

let Campground = mongoose.model("Campground", campSchema)

module.exports = Campground