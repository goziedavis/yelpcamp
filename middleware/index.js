//middleware functions go here
const Campground = require('../models/campground')
const Comment    = require('../models/comment')

let middlewareObj = {}

middlewareObj.isLoggedIn = function(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.flash('error', 'Please log in to continue')
    res.redirect('/login');
}

middlewareObj.checkCampgroundOwner = function(req, res, next) {
    Campground.findById(req.params.id, function(err, camp) {
        if(err) {
            res.redirect("back")
            console.log(err)
        } else {
            if(!camp) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            //check if user owns campground
            if(camp.author.id.equals(req.user._id)) {
                return next()
            } else {
                //res.send("YOU DO NOT HAVE PERMISSION TO DO THAT.")
                req.flash('error', 'You do not have the user permission to do that!')
                res.redirect("back")
            }
        }
    })
}

middlewareObj.checkCommentOwner = function(req, res, next) {
    Comment.findById(req.params.cid, function(err, comment) {
        if(err) {
            res.redirect("back")
            console.log(err)
        } else {
            if(!comment) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            //check if user owns campground
            if(comment.author.id.equals(req.user._id)) {
                return next()
            } else {
                //res.send("YOU DO NOT HAVE PERMISSION TO DO THAT.")
                req.flash('error', 'You do not have the user permission to do that!')
                res.redirect("back")
            }
        }
    })
}

module.exports = middlewareObj