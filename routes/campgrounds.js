const express       = require('express')
const router        = express.Router()
const Campground    = require('../models/campground')
const middlewareObj = require('../middleware/index')
//const { route } = require('./comments')

//shows a list of available campgrounds
router.get("/campgrounds", function(req, res) {
    //get campgrounds from db
    Campground.find({}, function(err, camps) {
        if(err) {
            console.log("Unable to retrieve campgrounds")
        } else {
            console.log("Succesfully retreived campgrounds")
            res.render('campgrounds/campgrounds', {campgrounds: camps})
        }
    })

    //res.render('campgrounds', {campgrounds: campgrounds})
})

//get data from form and add to campgrounds array
router.post("/campgrounds", middlewareObj.isLoggedIn, function(req, res) {
    let name = req.body.campName
    let img = req.body.imgUrl
    let desc = req.body.description
    let author = {
        id: req.user._id,
        username: req.user.username
    }
    let newCamp = {name: name, image: img, description: desc, author: author}

    //create new campground object and save to the db
    Campground.create(newCamp, function(err, camp) {
        if(err) {
            console.log("Unable to add campground to database")
            console.log(err)
        } else {
            if(!camp) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            //console.log(req.user)
            console.log("Successfully added campground to database")
            console.log(camp)
            req.flash('success', 'Successfully created new campground.')
        }
    })
    res.redirect('/campgrounds')
    //res.send("Campgrounds post request")
})

//contains form for creating new campground
router.get("/campgrounds/new", middlewareObj.isLoggedIn, function(req, res) {
    res.render('campgrounds/newCampGround')
})

//SHOW route
router.get("/campgrounds/:id", function(req, res) {
    console.log(req.params)
    Campground.findById(req.params.id).populate("comments").exec(function(err, camp) {
        if(err) {
            console.log("Unable to locate campground in database")
            console.log(err)
        } else {
            if(!camp) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            console.log("Successfully located campground in database")
            console.log(camp)
            res.render('campgrounds/show', {campground: camp})
        }
    })
    //res.render('show')
})

//EDIT campground route
router.get('/campgrounds/:id/edit', middlewareObj.isLoggedIn, middlewareObj.checkCampgroundOwner, function(req, res) {

    Campground.findById(req.params.id, function(err, camp) {
        if(!camp) {
            req.flash('error', 'Error 400: Item not found')
            return res.redirect('/campgrounds')
        }
        res.render('campgrounds/edit', {campground: camp})
    })
    
})

//UPDATE campground route
router.put('/campgrounds/:id', middlewareObj.isLoggedIn, middlewareObj.checkCampgroundOwner, function(req, res) {
    Campground.findByIdAndUpdate(req.params.id, req.body.camp, function(err, updatedCamp) {
        if(err) {
            console.log(err)
        } else {
            if(!updatedCamp) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            console.log("Successfully retrieved update post from DB.")
            //console.log(body)
            req.flash('success', 'Successfully edited ' + req.body.camp.name)
            res.redirect('/campgrounds/' + req.params.id)
        }
    })

    //res.send("Successfully updated!!!")
})

//DELETE campground route
/*router.delete('/campgrounds/:id', isLoggedIn, function(req, res) {
    Campground.findByIdAndRemove(req.params.id, function(err) {
        if (err) {
            console.log(err)
            res.redirect('/campgrounds')
        }
        res.redirect('/campgrounds')
    })

    //res.send("You are attempting to delete a campground.")
})*/

router.delete('/campgrounds/:id', middlewareObj.isLoggedIn, middlewareObj.checkCampgroundOwner, async(req, res) => {
    try {
        let foundCampground = await Campground.findById(req.params.id)
        await foundCampground.remove()
        req.flash('success', 'Successfully deleted campground')
        res.redirect("/campgrounds")
    } catch (error) {
        console.log(error.message)
        res.redirect("/campgrounds")
    }
})

module.exports = router