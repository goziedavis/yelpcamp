const express  = require('express')
const router   = express.Router()
const User     = require('../models/user')
const passport = require('passport')

router.get("/", function(req, res) {
    res.render('landing', {currentUser: req.user})
})

// =============
//  AUTH ROUTES
// =============
router.get('/register', function(req, res) {
    res.render('register')
})
    
router.post('/register', function(req, res) {
    let newUser = new User({username: req.body.username})
    User.register(newUser, req.body.password, function(err, user) {
        if(err) {
            console.log("Error message: " + err.message)
            req.flash("error", err.message)
            return res.render('register')
        }
        passport.authenticate('local')(req, res, function() {
            res.flash('success', "Welcome to YelpCamp, " + user.username)
            res.redirect('/campgrounds')
        })
    })
    
    //res.send("This is the sign in route")
})
    
router.get('/login', function(req, res) {
    res.render('login', {referer: req.headers.referer}) //send the path that you came from to the login form
})

router.post('/login', passport.authenticate('local', {
    //successRedirect: '/campgrounds',
    failureRedirect: '/login'
}),function(req, res) {
    req.flash('success', 'Hi there, ' + req.user.username)
    res.redirect(req.body.referer)  //then use it here
})

router.get('/logout', function(req, res) {
    req.logout()
    req.flash('success', 'Successfully logged out.')
    res.redirect('/campgrounds')
})

/*function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}*/

module.exports = router