const express    = require('express')
const router     = express.Router()
const Campground = require('../models/campground')
const Comment    = require('../models/comment')
const middlewareObj = require('../middleware/index')

//===================================
// COMMENTS ROUTES
//===================================

//NEW comment route
router.get('/campgrounds/:id/comments/new', middlewareObj.isLoggedIn, function(req, res) {
    let id = req.params.id
    Campground.findById(id, function(err, body) {
        if(err) {
            console.log(err)
        } else {
            if(!body) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }
            console.log(body)
            res.render('comments/new', {campground: body})
        }
    })
})

//CREATE comment route
router.post('/campgrounds/:id/comments', middlewareObj.isLoggedIn, function(req, res) {
    Campground.findById(req.params.id, function(err, campground) {
        if(err) {
            console.log(err)
            res.redirect('/campgrounds')
        } else {
            if(!campground) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }

            console.log("found campground to add comment to!")
            Comment.create(req.body.comment, function(err, comment) {
                if(err) {
                    console.log(err)
                    res.redirect('/campgrounds')
                } else {
                    if(!comment) {
                        req.flash('error', 'Error 400: Item not found')
                        return res.redirect('/campgrounds')
                    }

                    comment.author.id = req.user._id
                    comment.author.username = req.user.username
                    comment.save()
                    //console.log("The user who made this comment is: " + req.user)
                    campground.comments.push(comment)
                    campground.save()
                    console.log("Added new comment to campground")
                    req.flash('success', 'Review added successfully.')
                    res.redirect('/campgrounds/' + req.params.id)
                }
            })
        }
    })
})

//EDIT comment route
router.get('/campgrounds/:id/comments/:cid/edit', middlewareObj.isLoggedIn, middlewareObj.checkCommentOwner, function(req, res) {
    Campground.findById(req.params.id, function(err, camp) {
        if(err) {
            console.log(err)
            res.redirect("back")
        } else {
            if(!camp) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }

            Comment.findById(req.params.cid, function(err, comment) {
                if(err) {
                    console.log(err)
                    res.redirect("back")
                } else {
                    if(!comment) {
                        req.flash('error', 'Error 400: Item not found')
                        return res.redirect('/campgrounds')
                    }

                    res.render('comments/edit', {campground: camp, comment: comment})
                }
            })
        }
    })
})

//UPDATE comment route
router.post('/campgrounds/:id/comments/:cid', middlewareObj.isLoggedIn, middlewareObj.checkCommentOwner, function(req, res) {
    Comment.findByIdAndUpdate(req.params.cid, req.body.comment, function(err, comment) {
        if(err) {
            console.log(err)
            res.redirect("back")
        } else {
            if(!comment) {
                req.flash('error', 'Error 400: Item not found')
                return res.redirect('/campgrounds')
            }

            console.log("Comment update success")
            req.flash('success', 'Review updated successfully.')
            res.redirect('/campgrounds/' + req.params.id)
        }
    })

    //res.send("comment update route")
})

//DELETE comment route
router.delete('/campgrounds/:id/comments/:cid', middlewareObj.isLoggedIn, middlewareObj.checkCommentOwner, function(req, res) {
    Comment.findByIdAndRemove(req.params.cid, function(err) {
        if(err) {
            console.log(err)
            res.redirect('/campgrounds/' + req.params.id)
        }
        req.flash('success', 'Successfully removed review.')
        res.redirect('/campgrounds/' + req.params.id)
    })
})

module.exports = router