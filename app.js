const express               = require('express')
const app                   = express()
const bodyParser            = require('body-parser')
const mongoose              = require('mongoose')
const passport              = require('passport')
const expressSession        = require('express-session')
const flash                 = require('connect-flash')
const methodOverride        = require('method-override')
const LocalStrategy         = require('passport-local').Strategy
const passportLocalMongoose = require('passport-local-mongoose')
const Campground            = require('./models/campground')
const Comment               = require('./models/comment')
const User                  = require('./models/user')

let port = process.env.PORT || 3000
//const seedDB = require('./seed')

const campgroundRoutes      = require('./routes/campgrounds')
const commentRoutes         = require('./routes/comments')
const indexRoutes           = require('./routes/index')

//seedDB()
app.set("view engine", "ejs")
app.use(express.static(__dirname + "/public"))
app.use(bodyParser.urlencoded({extended: true}))
app.use(methodOverride('_method'))
app.use(flash())

//passport config
app.use(expressSession({
    secret: "This is supposed to be a secret, but I have no idea what to even put in here.",
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())   // encodes session data
passport.deserializeUser(User.deserializeUser())   // decodes session data

mongoose.connect('mongodb://localhost/yelp_camp', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => console.log('Connected to DB!'))
.catch(error => console.log(error.message))

//middleware that will be called on all routes
app.use(function(req, res, next) {
    res.locals.currentUser = req.user
    res.locals.errorFlash = req.flash("error")
    res.locals.success = req.flash("success")
    //res.locals.loginFlash = req.flash("login")
    next()
})

//use the imported routes
app.use(commentRoutes)
app.use(campgroundRoutes)
app.use(indexRoutes)

app.listen(port, function() {
    console.log("Running app...")
})